﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{

	public GameObject bulletPrefab;
	public Transform bulletSpawn;

	[SyncVar]
	public Color color;

	[Command]
	void CmdFire()
	{
		var bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

		bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;
		NetworkServer.Spawn(bullet);

		Destroy(bullet, 2.0f);
	}

	[ClientRpc]
	void RpcSetColor( Color newColor )
	{
		GetComponent<MeshRenderer>().material.color = newColor;
	}

	[Command]
	void CmdColor(Color newColor)
	{
		color = newColor;

		if (isServer)
		{
			RpcSetColor(color);
		}
	}

	void Update ()
	{
		if (!isLocalPlayer)
		{
			return;
		}

		var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
		var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

		transform.Rotate(0, x, 0);
		transform.Translate(0, 0, z);


		if (Input.GetKeyDown(KeyCode.Space))
		{
			CmdFire();
		}

		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
			CmdColor(color);
		}
	}

	public override void OnStartClient()
	{

		if (isServer)
		{
			color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
			RpcSetColor(color);
		}

	}
}
