﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Health : NetworkBehaviour
{

	public const int maxHealth = 100;

	public RectTransform healthBar;
	public bool destroyOnDeath;
	public NetworkStartPosition networkStartPosition1;
	public NetworkStartPosition networkStartPosition2;

	[SyncVar(hook = "OnChangeHealth")]
	public int currentHealth = maxHealth;

	public void TakeDamage(int amount)
	{
		if (!isServer)
		{
			return;
		}

		healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
		currentHealth -= amount;
		if (currentHealth<=0)
		{
			currentHealth = maxHealth;
			RpcRespawn();
		}
	}

	void OnChangeHealth(int health)
	{
		healthBar.sizeDelta = new Vector2(health,healthBar.sizeDelta.y);
	}

	[ClientRpc]
	void RpcRespawn()
	{
		if (destroyOnDeath)
		{
			Destroy(gameObject);
		}
		else
		{
			var networkStartPositionList = FindObjectsOfType<NetworkStartPosition>();
			var randomIndex = Random.Range(0, networkStartPositionList.Length);

			Debug.Log(randomIndex);
			var networkStartPosition = networkStartPositionList[randomIndex];

			transform.position = networkStartPosition.transform.position;
		}

	}
}
